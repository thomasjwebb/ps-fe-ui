import 'bootstrap/dist/css/bootstrap.min.css';
import App from './components/App.svelte';

const app = new App({
  target: document.body,
  props: {
    name: 'world',
  },
});

window.app = app;

export default app;
