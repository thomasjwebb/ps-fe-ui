module.exports = {
	testMatch: ['**/*.test.js'],
	transform: {
		'^.+\\.js$': '<rootDir>/jest.transform.js',
		'^.+\\.svelte$': 'jest-transform-svelte'
	},
	moduleFileExtensions: ['js', 'svelte'],
	bail: false,
  moduleNameMapper: {
    '@/(.*)$': '<rootDir>/src/$1',
  },
	setupFilesAfterEnv: [
		"<rootDir>/jest.setup.js"
	]
};