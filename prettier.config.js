module.exports = {
    tabWidth: 2,
    semi: true,
    singleQuote: true,
    trailingComma: "all",
    pluginSearchDirs: ["src"],
    plugins: ["prettier-plugin-svelte"],
};
